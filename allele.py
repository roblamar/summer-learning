class Allele:
    def __init__(self,p):
      self.pair=p
    #true represents adenine+cytosine and false represents thymine+guanine
    def print (self):
      if self.pair==True:
        print ("AC")
      else:
        print("TG")
class DNAstrand:
    sequence= []
    def __init__ (self,s):
      self.sequence=[]
      for allele in s:
        a=Allele(allele)
        self.sequence.append(a)
    def print (self):
      for allele in self.sequence:
        allele.print ()