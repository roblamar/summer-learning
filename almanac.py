class Entry:
  def __init__(self,humidity, 
               species, 
               fertilizer, 
               sunlight,
               weeds):
    self.humid= humidity
    self.spec = species
    #fert just equals the amount of fertilizer in soil
    self.fert = fertilizer
    #here sunlight just means full or partial, add hours of sun later
    self.sunl = sunlight
    self.weed = weeds
  def print (self):
    print (self.spec)
    print(self.sunl)
    print(self.humid)
    print (self.fert)
    print (self.weed)


meplant= Entry(.75, "marigold", "56 grams","partial", "7 weeds"  )
print(meplant)